#-------------------------------------------------
#
# Project created by QtCreator 2010-09-23T22:41:31
#
#-------------------------------------------------

QT       += core

QT       -= gui

win32:INCLUDEPATH = T:\source\mattie\WpdPack\Include

win32:LIBS += -LT:\source\mattie\WpdPack\Lib -lwpcap
unix:LIBS += -lpcap

TARGET = pcaptest
CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp
