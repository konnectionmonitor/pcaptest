#include <QtCore/QCoreApplication>

#include <QtDebug>

#include <pcap.h>
#include <string.h>
#include <stdlib.h>

#ifndef Q_WS_WIN
#include <sys/time.h>
#endif

#define MAXCAPTURE 65535


/*
struct timeval

    The struct timeval structure represents an elapsed time. It is declared in sys/time.h and has the following members:

    long int tv_sec
        This represents the number of whole seconds of elapsed time.
    long int tv_usec
        This is the rest of the elapsed time (a fraction of a second), represented as the number of microseconds. It is always less than one million.

*/
void processPacket(u_char *arg /*userdata*/, const struct pcap_pkthdr* pkthdr, const u_char * packet)
{
    Q_UNUSED(arg);
    Q_UNUSED(packet);
    static long last_sec = 0;
    static long load = 0, packetCount = 0;
    if (pkthdr->ts.tv_sec != last_sec) {
        printf("ts = %d, load = %.1f KB/s (%d)\n", pkthdr->ts.tv_sec,(float)load/1024.f, packetCount);
        last_sec = pkthdr->ts.tv_sec;
        packetCount = load = 0;
    }
    load += pkthdr->len;
    ++packetCount;
}

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    pcap_t *descriptor = NULL;
    char errbuf[PCAP_ERRBUF_SIZE];
    char *device=NULL;
    memset(errbuf,0,PCAP_ERRBUF_SIZE);

    pcap_if_t* deviceList = NULL;

    if (pcap_findalldevs(&deviceList, errbuf) == -1) {
        qDebug() << "failed to fetch device list";
    } else {
        pcap_if_t* dev = 0;
        for (dev = deviceList; dev; dev = dev->next) {
            if (dev->description) {
                qDebug() << dev->name << endl << dev->description;
            } else {
                qDebug() << "<noname>";
            }
        }
    }
    pcap_freealldevs(deviceList);

    if( argc > 1) {
        device = argv[1];
    } else {
        if ( (device = pcap_lookupdev(errbuf)) == NULL) {
            fprintf(stderr, "ERROR: %s\n", errbuf);
            exit(1);
        }
    }

    printf("Opening device %s\n", device);

    if ( (descriptor = pcap_open_live(device, MAXCAPTURE, 0 /* promisc */,  1000 /* ms */, errbuf)) == NULL) {
        fprintf(stderr, "ERROR: %s\n", errbuf);
        exit(1);
    }

    if ( pcap_loop(descriptor, -1 /*loop until err*/, processPacket, NULL) == -1) {
        fprintf(stderr, "ERROR: %s\n", pcap_geterr(descriptor) );
        exit(1);
    }

    return 0;
}
